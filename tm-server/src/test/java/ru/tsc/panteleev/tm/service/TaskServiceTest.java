package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.service.ITaskService;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @NotNull
    private final User user = new User();

    @NotNull
    private final static String STRING_RANDOM = UUID.randomUUID().toString();

    @NotNull
    private final static String STRING_EMPTY = "";

    @Nullable
    private final static String STRING_NULL = null;

    private final int COUNT_TEST_TASK = 666;

    @After
    public void finalization() {
        repository.clear();
    }

    public void addTestRecords() {
        for (int i = 0; i < COUNT_TEST_TASK; i++)
            service.add(user.getId(), new Task());
    }

    @Test
    public void add() {
        Assert.assertEquals(0, service.getSize(user.getId()));
        Assert.assertNotNull(service.add(user.getId(), new Task()));
        Assert.assertEquals(1, service.getSize(user.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(STRING_EMPTY, new Task()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(STRING_NULL, new Task()));
        Assert.assertNull(service.add(user.getId(), null));
        Assert.assertEquals(1, service.getSize(user.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(0, service.getSize(user.getId()));
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_TASK, service.getSize(user.getId()));
        Assert.assertEquals(COUNT_TEST_TASK, service.findAll(user.getId()).size());
    }

    @Test
    public void findById() {
        Assert.assertEquals(0, service.getSize(user.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findById(STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findById(STRING_NULL, STRING_RANDOM));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(STRING_RANDOM, STRING_NULL));
        Task task = service.add(user.getId(), new Task());
        Assert.assertNotNull(service.findById(user.getId(), task.getId()));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.findById(user.getId(), STRING_RANDOM));
    }

    @Test
    public void findByIndex() {
        Assert.assertEquals(0, service.getSize(user.getId()));
        addTestRecords();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findByIndex(STRING_EMPTY, 1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findByIndex(STRING_NULL, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(STRING_RANDOM, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(user.getId(), -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(user.getId(), COUNT_TEST_TASK));
        Assert.assertNotNull(service.findByIndex(user.getId(), 0));
        Assert.assertNotNull(service.findByIndex(user.getId(), COUNT_TEST_TASK - 1));
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, service.getSize(user.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findById(STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findById(STRING_NULL, STRING_RANDOM));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(STRING_RANDOM, STRING_NULL));
        Task task = service.add(user.getId(), new Task());
        Assert.assertNull(service.removeById(user.getId(), STRING_RANDOM));
        Assert.assertNotNull(service.removeById(user.getId(), task.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, service.getSize(user.getId()));
        addTestRecords();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findByIndex(STRING_EMPTY, 1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findByIndex(STRING_NULL, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(STRING_RANDOM, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(user.getId(), -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(user.getId(), COUNT_TEST_TASK));
        Assert.assertNotNull(service.removeByIndex(user.getId(), COUNT_TEST_TASK - 1));
        Assert.assertEquals(COUNT_TEST_TASK - 1, service.getSize(user.getId()));
        Assert.assertNotNull(service.removeByIndex(user.getId(), 0));
        Assert.assertEquals(COUNT_TEST_TASK - 2, service.getSize(user.getId()));
    }

    @Test
    public void clear() {
        addTestRecords();
        service.clear();
        Assert.assertEquals(0, service.getSize(user.getId()));
    }

    @Test
    public void existsById() {
        Task task = service.add(user.getId(), new Task());
        Assert.assertFalse(service.existsById(user.getId(), STRING_RANDOM));
        Assert.assertTrue(service.existsById(user.getId(), task.getId()));
    }

    @Test
    public void getSize() {
        addTestRecords();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(STRING_EMPTY));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(STRING_NULL));
        Assert.assertEquals(COUNT_TEST_TASK, service.getSize(user.getId()));
    }

    @Test
    public void create() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final Task task = service.create(user.getId(), name, description);
        Assert.assertNotNull(task);
        Assert.assertSame(name, task.getName());
        Assert.assertSame(description, task.getDescription());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(STRING_EMPTY, STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(STRING_NULL, STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(user.getId(), STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(user.getId(), STRING_NULL, STRING_RANDOM));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(user.getId(), STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(user.getId(), STRING_RANDOM, STRING_NULL));
    }

    @Test
    public void updateByIndex() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final Task task = service.create(user.getId(), name, description);
        @NotNull final String updateName = UUID.randomUUID().toString();
        @NotNull final String updateDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(service.updateByIndex(user.getId(), 0, updateName, updateDescription));
        Assert.assertSame(updateName, task.getName());
        Assert.assertSame(updateDescription, task.getDescription());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(STRING_EMPTY, 0, STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(STRING_NULL, 0, STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(user.getId(), -1, STRING_RANDOM, STRING_RANDOM));
    }

    @Test
    public void updateById() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final Task task = service.create(user.getId(), name, description);
        @NotNull final String updateName = UUID.randomUUID().toString();
        @NotNull final String updateDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(service.updateById(user.getId(), task.getId(), updateName, updateDescription));
        Assert.assertSame(updateName, task.getName());
        Assert.assertSame(updateDescription, task.getDescription());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(STRING_EMPTY, task.getId(), STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(STRING_NULL, task.getId(), STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(user.getId(), STRING_EMPTY, STRING_RANDOM, STRING_RANDOM));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(user.getId(), STRING_NULL, STRING_RANDOM, STRING_RANDOM));
    }

    @Test
    public void changeStatusByIndex() {
        @NotNull final Task task = service.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertSame(Status.NOT_STARTED, task.getStatus());
        service.changeStatusByIndex(user.getId(), 0, Status.IN_PROGRESS);
        Assert.assertSame(Status.IN_PROGRESS, task.getStatus());
        service.changeStatusByIndex(user.getId(), 0, Status.COMPLETED);
        Assert.assertSame(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void changeStatusById() {
        @NotNull final Task task = service.create(user.getId(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertSame(Status.NOT_STARTED, task.getStatus());
        service.changeStatusById(user.getId(), task.getId(), Status.IN_PROGRESS);
        Assert.assertSame(Status.IN_PROGRESS, task.getStatus());
        service.changeStatusById(user.getId(), task.getId(), Status.COMPLETED);
        Assert.assertSame(Status.COMPLETED, task.getStatus());
    }

}

package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.model.User;
import java.util.List;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    private final int COUNT_TEST_PROJECT = 666;

    @NotNull
    private final User user = new User();

    @After
    public void finalization() {
        repository.clear();
    }

    public void addTestRecords() {
        for (int i = 0; i < COUNT_TEST_PROJECT; i++)
            repository.add(user.getId(), new Project());
    }

    @Test
    public void add() {
        Assert.assertEquals(0L, repository.getSize());
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_PROJECT, repository.getSize());
    }

    @Test
    public void findAll() {
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_PROJECT, repository.getSize());
        List<Project> findProjects = repository.findAll(user.getId());
        Assert.assertEquals(findProjects.size(), repository.getSize(user.getId()));
    }

    @Test
    public void findById() {
        Project project = new Project();
        String projectId = project.getId();
        repository.add(user.getId(), project);
        Assert.assertNotNull(repository.findById(user.getId(), projectId));
        Assert.assertNull(repository.findById(user.getId(), ""));
    }

    @Test
    public void findByIndex() {
        addTestRecords();
        Assert.assertNotNull(repository.findByIndex(user.getId(), 0));
        Assert.assertNotNull(repository.findByIndex(user.getId(), COUNT_TEST_PROJECT - 1));
    }

    @Test
    public void removeById() {
        addTestRecords();
        Project project = new Project();
        String projectId = project.getId();
        repository.add(user.getId(), project);
        Assert.assertEquals(COUNT_TEST_PROJECT + 1, repository.getSize(user.getId()));
        repository.removeById(projectId);
        Assert.assertEquals(COUNT_TEST_PROJECT, repository.getSize(user.getId()));
    }

    @Test
    public void removeByIndex() {
        addTestRecords();
        repository.removeByIndex(user.getId(), COUNT_TEST_PROJECT - 1);
        repository.removeByIndex(user.getId(), 0);
        Assert.assertEquals(COUNT_TEST_PROJECT - 2, repository.getSize(user.getId()));
    }

    @Test
    public void clear() {
        addTestRecords();
        repository.clear(user.getId());
        Assert.assertEquals(0L, repository.getSize(user.getId()));
    }

    @Test
    public void existsById() {
        Project project = new Project();
        String projectId = project.getId();
        repository.add(user.getId(), project);
        Assert.assertTrue(repository.existsById(user.getId(), projectId));
    }

}

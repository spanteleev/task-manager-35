package ru.tsc.panteleev.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.response.AbstractResponse;
import ru.tsc.panteleev.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<Task> tasks;

    public TaskShowListByProjectIdResponse(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }

}

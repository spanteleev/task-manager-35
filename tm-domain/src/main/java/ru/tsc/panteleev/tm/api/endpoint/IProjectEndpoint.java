package ru.tsc.panteleev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.project.*;
import ru.tsc.panteleev.tm.dto.response.project.*;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    @WebMethod
    ProjectCompleteByIdResponse completeProjectStatusById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    @WebMethod
    ProjectCompleteByIndexResponse completeProjectStatusByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    @WebMethod
    ProjectListResponse listProject(@NotNull ProjectListRequest request);

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectShowByIdResponse showProjectById(@NotNull ProjectGetByIdRequest request);

    @NotNull
    @WebMethod
    ProjectShowByIndexResponse showProjectByIndex(@NotNull ProjectGetByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    @WebMethod
    ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request);

}

package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.model.User;
import java.util.List;

public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    private final int COUNT_TEST_TASK = 666;

    @NotNull
    private final User user = new User();

    @After
    public void finalization() {
        repository.clear();
    }

    public void addTestRecords() {
        for (int i = 0; i < COUNT_TEST_TASK; i++)
            repository.add(user.getId(), new Task());
    }

    @Test
    public void add() {
        Assert.assertEquals(0L, repository.getSize());
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_TASK, repository.getSize());
    }

    @Test
    public void findAll() {
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_TASK, repository.getSize());
        List<Task> findTasks = repository.findAll();
        Assert.assertEquals(findTasks.size(), repository.getSize());
    }

    @Test
    public void findById() {
        Task task = new Task();
        String taskId = task.getId();
        repository.add(user.getId(), task);
        Assert.assertNotNull(repository.findById(taskId));
        Assert.assertNull(repository.findById(""));
    }

    @Test
    public void findByIndex() {
        addTestRecords();
        Assert.assertNotNull(repository.findByIndex(0));
        Assert.assertNotNull(repository.findByIndex(COUNT_TEST_TASK - 1));
    }

    @Test
    public void removeById() {
        addTestRecords();
        Task task = new Task();
        String taskId = task.getId();
        repository.add(user.getId(), task);
        Assert.assertEquals(COUNT_TEST_TASK + 1, repository.getSize(user.getId()));
        repository.removeById(taskId);
        Assert.assertNull(repository.removeById(taskId));
        Assert.assertEquals(COUNT_TEST_TASK, repository.getSize(user.getId()));
    }

    @Test
    public void removeByIndex() {
        addTestRecords();
        repository.removeByIndex(COUNT_TEST_TASK - 1);
        repository.removeByIndex(0);
        Assert.assertEquals(COUNT_TEST_TASK - 2, repository.getSize());
    }

    @Test
    public void clear() {
        addTestRecords();
        repository.clear(user.getId());
        Assert.assertEquals(0L, repository.getSize());
    }

    @Test
    public void existsById() {
        Task task = new Task();
        String taskId = task.getId();
        repository.add(user.getId(), task);
        Assert.assertTrue(repository.existsById(user.getId(), taskId));
    }

    @Test
    public void findAllByProjectId() {
        Project project = new Project();
        String projectId = project.getId();
        for (int i = 0; i < COUNT_TEST_TASK; i++) {
            Task task = new Task();
            task.setProjectId(projectId);
            task.setUserId(user.getId());
            repository.add(user.getId(), task);
        }
        addTestRecords();
        Assert.assertEquals(COUNT_TEST_TASK, repository.findAllByProjectId(user.getId(), projectId).size());
    }

}

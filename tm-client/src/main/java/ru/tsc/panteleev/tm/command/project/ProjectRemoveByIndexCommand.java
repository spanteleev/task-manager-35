package ru.tsc.panteleev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-index";

    @NotNull
    public static final String DESCRIPTION = "Remove project by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectEndpoint().removeProjectByIndex(new ProjectRemoveByIndexRequest(getToken(), index));
    }

}

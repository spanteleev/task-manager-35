package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.api.service.IUserService;
import ru.tsc.panteleev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.panteleev.tm.exception.entity.UserNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import ru.tsc.panteleev.tm.repository.UserRepository;
import java.util.UUID;

public class UserServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService service = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @NotNull
    private final static String STRING_RANDOM = UUID.randomUUID().toString();

    @NotNull
    private final static String STRING_EMPTY = "";

    @Nullable
    private final static String STRING_NULL = null;

    @After
    public void finalization() {
        service.clear();
    }

    @Test
    public void create() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginExistsException.class, () -> service.create(login, password, email));
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(STRING_EMPTY, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(STRING_NULL, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(STRING_RANDOM, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(STRING_RANDOM, STRING_NULL, STRING_EMPTY));
        Assert.assertThrows(EmailEmptyException.class, () -> service.create(STRING_RANDOM, STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(EmailEmptyException.class, () -> service.create(STRING_RANDOM, STRING_RANDOM, STRING_NULL));
        Assert.assertThrows(EmailExistsException.class, () -> service.create(STRING_RANDOM, STRING_RANDOM, email));
    }

    @Test
    public void findByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(STRING_NULL));
        Assert.assertNotNull(service.findByLogin(login));
    }

    @Test
    public void findByEmail() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(EmailEmptyException.class, () -> service.findByEmail(STRING_EMPTY));
        Assert.assertThrows(EmailEmptyException.class, () -> service.findByEmail(STRING_NULL));
        Assert.assertNotNull(service.findByEmail(email));
    }

    @Test
    public void isLoginExists() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertTrue(service.isLoginExists(login));
        Assert.assertFalse(service.isLoginExists(STRING_EMPTY));
        Assert.assertFalse(service.isLoginExists(STRING_NULL));
        Assert.assertFalse(service.isLoginExists(STRING_RANDOM));
    }

    @Test
    public void isEmailExists() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertTrue(service.isEmailExists(email));
        Assert.assertFalse(service.isEmailExists(STRING_EMPTY));
        Assert.assertFalse(service.isEmailExists(STRING_NULL));
        Assert.assertFalse(service.isEmailExists(STRING_RANDOM));
    }

    @Test
    public void removeByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertTrue(service.isLoginExists(login));
        Assert.assertNotNull(service.removeByLogin(login));
        Assert.assertFalse(service.isLoginExists(login));
        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(STRING_NULL));
    }

    @Test
    public void setPassword() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(IdEmptyException.class, () -> service.setPassword(STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(IdEmptyException.class, () -> service.setPassword(STRING_NULL, STRING_EMPTY));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.setPassword(user.getId(), STRING_EMPTY));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.setPassword(user.getId(), STRING_NULL));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.setPassword(STRING_RANDOM, STRING_RANDOM));
        Assert.assertNotNull(service.setPassword(user.getId(), STRING_RANDOM));
    }

    @Test
    public void updateUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(IdEmptyException.class, () -> service.updateUser(STRING_EMPTY, STRING_EMPTY, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateUser(STRING_NULL, STRING_EMPTY, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.updateUser(STRING_RANDOM, STRING_EMPTY, STRING_EMPTY, STRING_EMPTY));
        Assert.assertNotNull(service.updateUser(user.getId(), STRING_RANDOM, STRING_RANDOM, STRING_RANDOM));
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(STRING_NULL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.lockUserByLogin(STRING_RANDOM));
        Assert.assertFalse(user.getLocked());
        service.lockUserByLogin(login);
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final User user = service.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginEmptyException.class, () -> service.unlockUserByLogin(STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> service.unlockUserByLogin(STRING_NULL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.unlockUserByLogin(STRING_RANDOM));
        Assert.assertFalse(user.getLocked());
        service.lockUserByLogin(login);
        Assert.assertTrue(user.getLocked());
        service.unlockUserByLogin(login);
        Assert.assertFalse(user.getLocked());
    }

}
